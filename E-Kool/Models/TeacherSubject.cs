﻿using AspNetCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Kool.Models
{
    
    public class TeacherSubject
    {
        public int Id { get; set; }

        public int SubjectId { get; set; }
        public int TeacherId { get; set; }
       
        public virtual Subject Subject { get; set; }
        public virtual Teacher Teacher { get; set; }

    }
}
