﻿using AspNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E_Kool.Models
{
    public class SchoolClassSubjects
    {
        public int Id { get; set; }


        [Display(Name = "Õppeaine nimi")]
        public int SubjectId { get; set; }

        [Display(Name = "Klassi kood")]
        public int SchoolClassId { get; set; }

        [Display(Name = "Õppeaine nimi")]
        public virtual Subject Subject { get; set; }
        [Display(Name = "Klassi kood")]
        public virtual SchoolClass SchoolClass { get; set; }
    }
}
