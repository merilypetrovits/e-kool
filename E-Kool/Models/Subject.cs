﻿using E_Kool.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCore.Models
{
    public class Subject
    {
        [Display(Name = "Õppeaine kood")]
        public int Id { get; set; }
        public virtual ICollection<TeacherSubject> TeacherSubjects { get; set; }
        public virtual ICollection<SchoolClassSubjectTeacher> SchoolClassSubjectTeachers { get; set; }

        [Display(Name = "Õppeaine nimi")]
        public string SubjectName { get; set; }

      

    }
}
