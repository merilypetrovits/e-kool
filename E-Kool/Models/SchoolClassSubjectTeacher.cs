﻿using AspNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace E_Kool.Models
{
    public class SchoolClassSubjectTeacher
    {
      
        public int Id { get; set; }

        public int SubjectId { get; set; }
        public int SchoolClassId { get; set; }
        public int TeacherId { get; set; }

        [Display(Name = "Õppeaine nimi")]
        public virtual Subject Subject { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
        [Display(Name = "Õpetaja nimi")]
        public virtual Teacher Teacher { get; set; }

    }
}
