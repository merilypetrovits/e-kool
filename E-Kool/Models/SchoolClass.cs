﻿using E_Kool.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCore.Models
{
    public class SchoolClass
    {
        public virtual ICollection<SchoolClassSubjectTeacher> SchoolClassSubjectTeachers { get; set; }

        public int Id { get; set; }

        [Display(Name = "Klassi nimi")]
        public string ClassName { get; set; }

        [Display(Name = "Klassi kood")]
        public char ClassCode { get; set; }
    }
}
