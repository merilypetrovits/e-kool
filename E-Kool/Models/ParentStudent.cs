﻿using AspNetCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Kool.Models
{
    public class ParentStudent
    {
        public int Id { get; set; }

        public int StudentId { get; set; }
        public int ParentsId { get; set; }

        public virtual Student Student { get; set; }
        public virtual Parents Parents { get; set; }
    }
}
