﻿using AspNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace E_Kool.Models
{
    public class StudentSubjectGrades
    {
        public int Id { get; set; }

        public int StudentId { get; set; }
        public int SubjectId { get; set; }
        public int GradeId { get; set; }
        public int TeacherId { get; set; }




        [Display(Name = "Õpilase nimi")]
        public virtual Student Student { get; set; }
        [Display(Name = "Õppeaine nimi")]
        public virtual Subject Subject { get; set; }
        [Display(Name = "Hinne")]
        public virtual Grade Grade { get; set; }

        public virtual Teacher Teacher { get; set; }



    }
}
