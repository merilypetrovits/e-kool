﻿using E_Kool.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCore.Models
{
    public class Teacher
    {
        public int Id { get; set; }
        [Display(Name = "Õppeaine")]
        public virtual List<TeacherSubject> TeacherSubject { get; set; }
        [Display(Name = "Klass")]
        public virtual ICollection<SchoolClassSubjectTeacher> SchoolClassSubjectTeachers { get; set; }

        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }

        [Display(Name = "Perekonnanimi")]
        public string LastName { get; set; }

        [Display(Name = "Isikukood")]
        public string IdCode { get; set; }


        public string Email { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public string ApplicationUserId { get; set; }


        [Display(Name = "Rolli nimi")]
        public string RoleName { get; set; }

        [NotMapped]
        public ICollection<int> SelectedSubjectIds { get; set; }

    }
}
