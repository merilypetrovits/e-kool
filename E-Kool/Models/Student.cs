﻿using E_Kool.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AspNetCore.Models
{
    public class Student
    {
        public virtual ICollection<ParentStudent> ParentStudents { get; set; }
        public int Id { get; set; }

      
        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }

        [Display(Name = "Perekonnanimi")]
        public string LastName { get; set; }

        [Display(Name = "Isikukood")]
        public string IdCode { get; set; }

        public string Email { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public string ApplicationUserId { get; set; }

        [Display(Name = "Klass")]
        public int SchoolClassId { get; set; }
        public SchoolClass SchoolClass { get; set; }

        [Display(Name = "Class Code")]
        public string ClassCode { get; set; }

        [Display(Name = "Õppeaine nimi")]
        public string SubjectsName { get; set; }

        [Display(Name = "Hinne I")]
        public string GradeOne { get; set; }

        [NotMapped]
        [Display(Name = "Keskmine hinne")]
        public double AverageGrade { get; set; }
    }
}
