using System;

namespace E_Kool.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }



        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}