﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace E_Kool.Models
//namespace AspNetCore.Models
{
    public class Grade
    {
        public int Id { get; set; }

        [Display(Name = "Hinne")]
        public int GradeValue { get; set; }
        [Display(Name = "Kirjeldus")]
        public string GradeName { get; set; }
        
    }
}
