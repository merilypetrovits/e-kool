﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using E_Kool.Models;
using E_Kool.Data;
using AspNetCore.Models;

namespace E_Kool.Controllers
{
    public class HomeController : Controller
    {

        private readonly ApplicationDbContext _context;
        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Teachers()
        {
            List<Teacher> teachers = _context.Teacher
                .OrderBy(x => x.FirstName)
                .ThenBy(x => x.LastName)
                .ToList();
            return View(teachers);
        }


        public IActionResult Students()
        {
            List<Student> students = _context.Student
                 .OrderBy(x => x.FirstName)
                 .ThenBy(x => x.LastName)
                 .ToList();

            return View(students);
        }
        public IActionResult Subjects()
            {
            var subjects = _context.Subject
          .OrderBy(x => x.Id)
          .ToList();

            return View(subjects);
            }
        public IActionResult Classes()
        {
            var schoolClasses = _context.SchoolClass
              .OrderBy(x => x.ClassCode)
              .ToList();

            return View(schoolClasses);
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
