﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCore.Models;
using E_Kool.Data;
using Microsoft.AspNetCore.Identity;
using E_Kool.Models;

namespace E_Kool.Controllers
{
    public class TeachersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        

        public TeachersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;

        }

        // GET: Teachers
        public async Task<IActionResult> Index()
        {

            List<Teacher> teachers = await _context.Teacher
               .Include(x => x.TeacherSubject)
               .ThenInclude(x => x.Subject)
               .OrderBy(x => x.FirstName)
               .ThenBy(x => x.LastName)
               .ThenBy(x => x.RoleName)
               .ToListAsync();

           
            return View(teachers);
        }

        // GET: Teachers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teacher
                .FirstOrDefaultAsync(m => m.Id == id);
            if (teacher == null)
            {
                return NotFound();
            }

            if (User.IsInRole("Admin") || User.IsInRole("Headteacher")) //See koodijupp pange juurde, kui tahate kellelegi mingi tegevuse jaoks õiguse anda
            {
                ViewData["RoleNames"] = new SelectList(_context.Roles.Where(x => x.Name != "Student" && x.Name != "Parent"), "Name", "Name");
               
                return View(teacher);
            }
            else
            {
                return RedirectToAction("Index");
            }

            
        }

        // GET: Teachers/Create
        public IActionResult Create()
        {
            if (User.IsInRole("Admin") || User.IsInRole("Headteacher") || User.IsInRole("Principle"))
            {
                ViewData["RoleNames"] = new SelectList(_context.Roles.Where(x => x.Name != "Student" && x.Name != "Parent"), "Name", "Name");



                ViewBag.SubjectList = new MultiSelectList(_context.Subject.Select(x => new SelectListItem()
                {
                    Text = x.SubjectName,
                    Value = x.Id.ToString()
                }), "Value","Text");

                return View();

            }
            else
            {
                return Content("Ainult admin ja õppealajuhataja saavad õpetajaid/direktoreid/õppealjuhatajaid lisada");
            }
        }

        // POST: Teachers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,IdCode,Email,RoleName,SubjectName,SelectedSubjectIds")] Teacher teacher)
        {

            if (ModelState.IsValid)
            {

                var user = new ApplicationUser()
                {
                    UserName = teacher.Email,// sama mis email
                    Email = teacher.Email,
                };
                await _userManager.CreateAsync(user, "A123!a");

                await _userManager.AddToRoleAsync(user, teacher.RoleName); // kasutajale läheb adminni roll

                teacher.ApplicationUserId = user.Id;
                _context.Add(teacher);
                
                await _context.SaveChangesAsync();

                if (teacher.SelectedSubjectIds != null)
                {
                    foreach (var subjectId in teacher.SelectedSubjectIds)
                    {
                        var teacherSubject = new TeacherSubject()
                        {
                            SubjectId = subjectId,
                            TeacherId = teacher.Id
                        };
                        await _context.AddAsync(teacherSubject);
                    }
                }

                await _context.SaveChangesAsync();


                return RedirectToAction(nameof(Index));
            }
            return View(teacher);
        }

        // GET: Teachers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teacher.Include(x => x.TeacherSubject)
               .ThenInclude(x => x.Subject).Where(x => x.Id == id).FirstOrDefaultAsync();//üks õpeteja , list annab mitu
            if (teacher == null)
            {
                return NotFound();
            }

            if (User.IsInRole("Headteacher"))
            {
                
                ViewData["RoleNames"] = new SelectList(_context.Roles.Where(x => x.Name != "Student" && x.Name != "Parent"), "Name", "Name");
              
                ViewBag.SubjectList = new MultiSelectList(_context.Subject.Select(x => new SelectListItem()
                {
                    Text = x.SubjectName,
                    Value = x.Id.ToString()
                }), "Value", "Text");

               
                return View(teacher);
            }
            else
            {
                return Content("Ainult õppealajuhataja saab aineid, klasse jms muuta");
            }

        }

        // POST: Teachers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,IdCode,Email,RoleName,SubjectName,SelectedSubjectIds")] Teacher teacher)
        {
            if (id != teacher.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(teacher);
                    var existingSubjectIds = _context.TeacherSubject.Where(x => x.TeacherId == teacher.Id).Select(x => x.SubjectId).ToList();
                    if (teacher.SelectedSubjectIds != null)
                    {
                        
                        var subjectIdsToAdd = teacher.SelectedSubjectIds.Except(existingSubjectIds);
                        foreach (var subjectId in subjectIdsToAdd)
                        {
                            var subjectTeacher = new TeacherSubject()
                            {
                                SubjectId = subjectId,
                                TeacherId = teacher.Id
                            };
                            await _context.AddAsync(subjectTeacher);
                        }
                    }
                    if (existingSubjectIds != null)
                    {
                        if (teacher.SelectedSubjectIds == null)
                        {
                            teacher.SelectedSubjectIds = new List<int>();
                        }
                        var subjectIdsToDelete = existingSubjectIds.Except(teacher.SelectedSubjectIds);
                        foreach (var subjectId in subjectIdsToDelete)
                        {
                            var subjectTeacher = _context.TeacherSubject.Where(x => x.SubjectId == subjectId && x.TeacherId == teacher.Id).FirstOrDefault();
                            _context.Remove(subjectTeacher);
                        }
                    }
                    _context.Update(teacher);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TeacherExists(teacher.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(teacher);
        }

        // GET: Teachers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teacher
                .FirstOrDefaultAsync(m => m.Id == id);
            if (teacher == null)
            {
                return NotFound();
            }

            return View(teacher);
        }

        // POST: Teachers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var teacher = await _context.Teacher.FindAsync(id);
            _context.Teacher.Remove(teacher);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TeacherExists(int id)
        {
            return _context.Teacher.Any(e => e.Id == id);
        }
    }
}
