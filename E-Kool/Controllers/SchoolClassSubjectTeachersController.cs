﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using E_Kool.Data;
using E_Kool.Models;
using System.Security.Claims;

namespace E_Kool.Controllers
{
    public class SchoolClassSubjectTeachersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SchoolClassSubjectTeachersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SchoolClassSubjectTeachers
        public async Task<IActionResult> Index()
        {
            List<SchoolClassSubjectTeacher> schoolClassSubjectTeacher;

            if (User.IsInRole("Student"))
            {
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var student = _context.Student.Where(x => x.ApplicationUserId == userId).FirstOrDefault();
                schoolClassSubjectTeacher = await _context.SchoolClassSubjectTeacher
                .Include(x => x.SchoolClass)
                .Include(x => x.Subject)
                .Include(x => x.Teacher)
                 .Where(x => x.SchoolClass.Id == student.SchoolClassId)
                //.Where(x => x.Teacher.ApplicationUser.Id == userId)
                .ToListAsync();

            }
            else
            {
                schoolClassSubjectTeacher = await _context.SchoolClassSubjectTeacher
                .Include(x => x.SchoolClass)
                .Include(x => x.Subject)
                .Include(x => x.Teacher)
                .ToListAsync();
            }

            return View(schoolClassSubjectTeacher);

        }

        // GET: SchoolClassSubjectTeachers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClassSubjectTeacher = await _context.SchoolClassSubjectTeacher
                .Include(s => s.SchoolClass.ClassCode)
                .Include(s => s.Subject.SubjectName)
                .Include(s => s.Teacher.FirstName)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolClassSubjectTeacher == null)
            {
                return NotFound();
            }

            return View(schoolClassSubjectTeacher);
        }

        // GET: SchoolClassSubjectTeachers/Create
        public IActionResult Create()
        {
            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode");
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName");
            ViewData["TeacherId"] = new SelectList(_context.Teacher, "Id", "FirstName");
            return View();
        }

        // POST: SchoolClassSubjectTeachers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,SubjectId,SchoolClassId,TeacherId")] SchoolClassSubjectTeacher schoolClassSubjectTeacher)
        {
            if (ModelState.IsValid)
            {
                _context.Add(schoolClassSubjectTeacher);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode", schoolClassSubjectTeacher.SchoolClassId);
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName", schoolClassSubjectTeacher.SubjectId);
            ViewData["TeacherId"] = new SelectList(_context.Teacher, "Id", "FirstName", schoolClassSubjectTeacher.TeacherId);
            return View(schoolClassSubjectTeacher);
        }

        // GET: SchoolClassSubjectTeachers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClassSubjectTeacher = await _context.SchoolClassSubjectTeacher.FindAsync(id);
            if (schoolClassSubjectTeacher == null)
            {
                return NotFound();
            }
            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode", schoolClassSubjectTeacher.SchoolClassId);
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName", schoolClassSubjectTeacher.SubjectId);
            ViewData["TeacherId"] = new SelectList(_context.Teacher, "Id", "FirstName", schoolClassSubjectTeacher.TeacherId);
            return View(schoolClassSubjectTeacher);
        }

        // POST: SchoolClassSubjectTeachers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SubjectId,SchoolClassId,TeacherId")] SchoolClassSubjectTeacher schoolClassSubjectTeacher)
        {
            if (id != schoolClassSubjectTeacher.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(schoolClassSubjectTeacher);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolClassSubjectTeacherExists(schoolClassSubjectTeacher.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode", schoolClassSubjectTeacher.SchoolClassId);
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName", schoolClassSubjectTeacher.SubjectId);
            ViewData["TeacherId"] = new SelectList(_context.Teacher, "Id", "FirstName", schoolClassSubjectTeacher.TeacherId);
            return View(schoolClassSubjectTeacher);
        }

        // GET: SchoolClassSubjectTeachers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClassSubjectTeacher = await _context.SchoolClassSubjectTeacher
                .Include(s => s.SchoolClass)
                .Include(s => s.Subject)
                .Include(s => s.Teacher)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolClassSubjectTeacher == null)
            {
                return NotFound();
            }

            return View(schoolClassSubjectTeacher);
        }

        // POST: SchoolClassSubjectTeachers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var schoolClassSubjectTeacher = await _context.SchoolClassSubjectTeacher.FindAsync(id);
            _context.SchoolClassSubjectTeacher.Remove(schoolClassSubjectTeacher);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SchoolClassSubjectTeacherExists(int id)
        {
            return _context.SchoolClassSubjectTeacher.Any(e => e.Id == id);
        }
    }
}
