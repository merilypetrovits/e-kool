﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCore.Models;
using E_Kool.Data;
using E_Kool.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace E_Kool.Controllers
{

    [Authorize]
    public class StudentsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public StudentsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Students
        public async Task<IActionResult> Index()
        {
            List<Student> students;

            // Lauri kood

            if (User.IsInRole("Student"))
            {
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                
                var currentUserWithClass = _context.Student.Include(x => x.SchoolClass).Where(x => x.ApplicationUserId == userId).FirstOrDefault();

                students = await _context.Student
                 .Include(x => x.SchoolClass)
                 .Where(x => x.SchoolClass.Id == currentUserWithClass.SchoolClass.Id)
                 .ToListAsync();


            }
            else if (User.IsInRole("Headteacher") || User.IsInRole("Principle"))
            {
                students = await _context.Student
                 .Include(x => x.SchoolClass)
                 .OrderBy(x => x.SchoolClass)

                 .ToListAsync();
                foreach (var student in students)
                {
                    var studentGrades = _context.StudentSubjectGrades
                         .Include(x => x.Grade)
                         .Where(x => x.StudentId == student.Id && x.Grade.GradeValue != 0).ToList();
                    if(studentGrades.Count > 0)
                    {
                        student.AverageGrade = Math.Round(studentGrades.Average(x => x.Grade.GradeValue), 2);

                    }

                }
            }
            else
            {
                 students = await _context.Student
                     .Include(x => x.SchoolClass)
                     .ToListAsync();
            }



            return View(students);
        }


        // GET: Students/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Student
                .FirstOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }
            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode");
            ViewData["SubjectsId"] = new SelectList(_context.Subject, "Id", "SubjectName");
            return View(student);
        }



        // GET: Students/Create


        public IActionResult Create()
        {
            if (User.IsInRole("Admin")) //See koodijupp pange juurde, kui tahate kellelegi mingi tegevuse jaoks õiguse anda
            {
                ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode");
                ViewData["SubjectsId"] = new SelectList(_context.Subject, "Id", "SubjectName");
                return View();

            }
            else
            {
                return Content("Ainult admin saab õpilasi lisada");
            }


        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,Email,LastName,IdCode,SchoolClassId,SubjectsId,GradeOne")] Student student)
        {
            if (ModelState.IsValid)
            {

                var user = new ApplicationUser()
                {

                    UserName = student.Email,// sama mis email
                    Email = student.Email,
                };
                await _userManager.CreateAsync(user, "A123!a");// määratud parool igale õpilasele
                await _userManager.AddToRoleAsync(user, "Student"); // kasutajale läheb adminni roll

                student.ApplicationUserId = user.Id;

                _context.Add(student);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }


            List<Student> students = _context.Student
                  .Include(x => x.SchoolClass)
                  .OrderBy(x => x.FirstName)
           .ToList();
            return View(students);
        }

        // GET: Students/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Student.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode");
            ViewData["SubjectsId"] = new SelectList(_context.Subject, "SubjectId", "SubjectName");
            return View(student);


        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,IdCode,SchoolClassId,SubjectsId,GradeOne")] Student student)
        {
            if (id != student.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(student);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(student);
        }

        // GET: Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Student
                .FirstOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = await _context.Student.FindAsync(id);
            _context.Student.Remove(student);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentExists(int id)
        {
            return _context.Student.Any(e => e.Id == id);
        }
    }


}

