﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using E_Kool.Data;
using E_Kool.Models;

namespace E_Kool.Controllers
{
    public class PrinciplesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PrinciplesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Principles
        public async Task<IActionResult> Index()
        {
            return View(await _context.Principle.ToListAsync());
        }

        // GET: Principles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var principle = await _context.Principle
                .FirstOrDefaultAsync(m => m.Id == id);
            if (principle == null)
            {
                return NotFound();
            }

            return View(principle);
        }

        // GET: Principles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Principles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,IdCode")] Principle principle)
        {
            if (ModelState.IsValid)
            {
                _context.Add(principle);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(principle);
        }

        // GET: Principles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var principle = await _context.Principle.FindAsync(id);
            if (principle == null)
            {
                return NotFound();
            }
            return View(principle);
        }

        // POST: Principles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,IdCode")] Principle principle)
        {
            if (id != principle.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(principle);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PrincipleExists(principle.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(principle);
        }

        // GET: Principles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var principle = await _context.Principle
                .FirstOrDefaultAsync(m => m.Id == id);
            if (principle == null)
            {
                return NotFound();
            }

            return View(principle);
        }

        // POST: Principles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var principle = await _context.Principle.FindAsync(id);
            _context.Principle.Remove(principle);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PrincipleExists(int id)
        {
            return _context.Principle.Any(e => e.Id == id);
        }
    }
}
