﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using E_Kool.Data;
using E_Kool.Models;
using AspNetCore.Models;
using System.Security.Claims;

namespace E_Kool.Controllers
{
    public class StudentSubjectGradesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StudentSubjectGradesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: StudentSubjectGrades
        public async Task<IActionResult> Index()
        {
            List<StudentSubjectGrades> studentSubjectGrades;


            if (User.IsInRole("Student"))
            {
                // õpilase vaade

                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

                studentSubjectGrades = await _context.StudentSubjectGrades
                    .Include(x => x.Student)
                    .ThenInclude(x => x.ApplicationUser)
                    .Where(x => x.Student.ApplicationUser.Id == userId)
                    .Include(x => x.Grade)
                    .Include(x => x.Subject)
                    .Where(x => x.Grade.GradeValue != 0)
                    .ToListAsync();

                if (studentSubjectGrades.Count > 0)
                {
                    ViewBag.AverageGrade = studentSubjectGrades/*.Where(x => x.Grade.GradeValue != 0)*/.Average(x => x.Grade.GradeValue);

                }
                else
                {
                    ViewBag.AverageGrade = 0;
                }

            }
            else if (User.IsInRole("Teacher"))
            {
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                

                studentSubjectGrades = await _context.StudentSubjectGrades
                .Include(x => x.Grade)
                .Include(x => x.Student)
                .Include(x => x.Subject)
                .Include(x => x.Teacher)
                .Where(x => x.Teacher.ApplicationUser.Id == userId)
          

                .ToListAsync();



            }
            else
            {
                studentSubjectGrades = await _context.StudentSubjectGrades
                .Include(s => s.Grade)
                .Include(s => s.Student)
                .Include(s => s.Subject)
                .ToListAsync();

            }

            //ViewBag.AverageGrade = studentSubjectGrades.Where(x => x.Grade.GradeValue != 0).Average(x => x.Grade.GradeValue);
            //var applicationDbContext = _context.StudentSubjectGrades.Include(s => s.Grade).Include(s => s.Student).Include(s => s.Subject);
            //return View(await applicationDbContext.ToListAsync());
            return View(studentSubjectGrades);

        }

        // GET: StudentSubjectGrades/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentSubjectGrades = await _context.StudentSubjectGrades
                .Include(s => s.Grade)
                .Include(s => s.Student)
                .Include(s => s.Subject)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (studentSubjectGrades == null)
            {
                return NotFound();
            }

            return View(studentSubjectGrades);
        }

        // GET: StudentSubjectGrades/Create
        public IActionResult Create()
        {
            ViewData["GradeId"] = new SelectList(_context.Grade, "Id", "GradeValue");
            ViewData["StudentId"] = new SelectList(_context.Student, "Id", "FirstName");


            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var teacher = _context.Teacher.Where(x => x.ApplicationUserId == userId).FirstOrDefault();

            var teacherSubjectIds = _context.TeacherSubject.Where(x => x.TeacherId == teacher.Id).Select(x => x.SubjectId);

            var subjects = _context.Subject.Where(x => teacherSubjectIds.Contains(x.Id))
             .ToList();

            ViewData["SubjectId"] = new SelectList(subjects, "Id", "SubjectName");
            return View();
        }

        // POST: StudentSubjectGrades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,StudentId,SubjectId,GradeId")] StudentSubjectGrades studentSubjectGrades)
        {
            if (ModelState.IsValid)
            {
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var teacher = _context.Teacher.Where(x => x.ApplicationUserId == userId).FirstOrDefault();

                studentSubjectGrades.TeacherId = teacher.Id;
                _context.Add(studentSubjectGrades);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GradeId"] = new SelectList(_context.Grade, "Id", "GradeValue", studentSubjectGrades.GradeId);
            ViewData["StudentId"] = new SelectList(_context.Student, "Id", "FirstName", studentSubjectGrades.StudentId);
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName", studentSubjectGrades.SubjectId);
            return View(studentSubjectGrades);
        }

        // GET: StudentSubjectGrades/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentSubjectGrades = await _context.StudentSubjectGrades.FindAsync(id);
            if (studentSubjectGrades == null)
            {
                return NotFound();
            }
            ViewData["GradeId"] = new SelectList(_context.Grade, "Id", "GradeValue", studentSubjectGrades.GradeId);
            ViewData["StudentId"] = new SelectList(_context.Student, "Id", "FirstName", studentSubjectGrades.StudentId);
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName", studentSubjectGrades.SubjectId);
            return View(studentSubjectGrades);
        }

        // POST: StudentSubjectGrades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,StudentId,SubjectId,GradeId")] StudentSubjectGrades studentSubjectGrades)
        {
            if (id != studentSubjectGrades.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                    var teacher = _context.Teacher.Where(x => x.ApplicationUserId == userId).FirstOrDefault();

                    studentSubjectGrades.TeacherId = teacher.Id;
                    _context.Update(studentSubjectGrades);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentSubjectGradesExists(studentSubjectGrades.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GradeId"] = new SelectList(_context.Grade, "Id", "GradeValue", studentSubjectGrades.GradeId);
            ViewData["StudentId"] = new SelectList(_context.Student, "Id", "FirstName", studentSubjectGrades.StudentId);
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName", studentSubjectGrades.SubjectId);
            return View(studentSubjectGrades);
        }

        // GET: StudentSubjectGrades/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentSubjectGrades = await _context.StudentSubjectGrades
                .Include(s => s.Grade)
                .Include(s => s.Student)
                .Include(s => s.Subject)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (studentSubjectGrades == null)
            {
                return NotFound();
            }

            return View(studentSubjectGrades);
        }

        // POST: StudentSubjectGrades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var studentSubjectGrades = await _context.StudentSubjectGrades.FindAsync(id);
            _context.StudentSubjectGrades.Remove(studentSubjectGrades);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentSubjectGradesExists(int id)
        {
            return _context.StudentSubjectGrades.Any(e => e.Id == id);
        }
    }
}
