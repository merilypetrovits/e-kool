﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using E_Kool.Data;
using E_Kool.Models;

namespace E_Kool.Controllers
{
    public class SchoolClassSubjectsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SchoolClassSubjectsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SchoolClassSubjects
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.SchoolClassSubjects
                .Include(s => s.SchoolClass)
                .OrderBy(s => s.SchoolClass)
                .Include(s => s.Subject);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: SchoolClassSubjects/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClassSubjects = await _context.SchoolClassSubjects
                .Include(s => s.SchoolClass.ClassCode)
                .Include(s => s.Subject.SubjectName)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolClassSubjects == null)
            {
                return NotFound();
            }

            return View(schoolClassSubjects);
        }

        // GET: SchoolClassSubjects/Create
        public IActionResult Create()
        {
            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode");
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName");
            return View();
        }

        // POST: SchoolClassSubjects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,SubjectId,SchoolClassId")] SchoolClassSubjects schoolClassSubjects)
        {
            if (ModelState.IsValid)
            {
                _context.Add(schoolClassSubjects);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode", schoolClassSubjects.SchoolClassId);
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName", schoolClassSubjects.SubjectId);
            return View(schoolClassSubjects);
        }

        // GET: SchoolClassSubjects/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClassSubjects = await _context.SchoolClassSubjects.FindAsync(id);
            if (schoolClassSubjects == null)
            {
                return NotFound();
            }
            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode", schoolClassSubjects.SchoolClassId);
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName", schoolClassSubjects.SubjectId);
            return View(schoolClassSubjects);
        }

        // POST: SchoolClassSubjects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SubjectId,SchoolClassId")] SchoolClassSubjects schoolClassSubjects)
        {
            if (id != schoolClassSubjects.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(schoolClassSubjects);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolClassSubjectsExists(schoolClassSubjects.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SchoolClassId"] = new SelectList(_context.SchoolClass, "Id", "ClassCode", schoolClassSubjects.SchoolClassId);
            ViewData["SubjectId"] = new SelectList(_context.Subject, "Id", "SubjectName", schoolClassSubjects.SubjectId);
            return View(schoolClassSubjects);
        }

        // GET: SchoolClassSubjects/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClassSubjects = await _context.SchoolClassSubjects
                .Include(s => s.SchoolClass)
                .Include(s => s.Subject)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolClassSubjects == null)
            {
                return NotFound();
            }

            return View(schoolClassSubjects);
        }

        // POST: SchoolClassSubjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var schoolClassSubjects = await _context.SchoolClassSubjects.FindAsync(id);
            _context.SchoolClassSubjects.Remove(schoolClassSubjects);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SchoolClassSubjectsExists(int id)
        {
            return _context.SchoolClassSubjects.Any(e => e.Id == id);
        }
    }
}
