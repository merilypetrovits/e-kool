﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using E_Kool.Data;
using E_Kool.Models;

namespace E_Kool.Controllers
{
    public class HeadTeachersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HeadTeachersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HeadTeachers
        public async Task<IActionResult> Index()
        {
            return View(await _context.HeadTeacher.ToListAsync());
        }

        // GET: HeadTeachers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var headTeacher = await _context.HeadTeacher
                .FirstOrDefaultAsync(m => m.Id == id);
            if (headTeacher == null)
            {
                return NotFound();
            }

            return View(headTeacher);
        }

        // GET: HeadTeachers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: HeadTeachers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,IdCode")] HeadTeacher headTeacher)
        {
            if (ModelState.IsValid)
            {
                _context.Add(headTeacher);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(headTeacher);
        }

        // GET: HeadTeachers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var headTeacher = await _context.HeadTeacher.FindAsync(id);
            if (headTeacher == null)
            {
                return NotFound();
            }
            return View(headTeacher);
        }

        // POST: HeadTeachers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,IdCode")] HeadTeacher headTeacher)
        {
            if (id != headTeacher.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(headTeacher);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HeadTeacherExists(headTeacher.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(headTeacher);
        }

        // GET: HeadTeachers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var headTeacher = await _context.HeadTeacher
                .FirstOrDefaultAsync(m => m.Id == id);
            if (headTeacher == null)
            {
                return NotFound();
            }

            return View(headTeacher);
        }

        // POST: HeadTeachers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var headTeacher = await _context.HeadTeacher.FindAsync(id);
            _context.HeadTeacher.Remove(headTeacher);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HeadTeacherExists(int id)
        {
            return _context.HeadTeacher.Any(e => e.Id == id);
        }
    }
}
