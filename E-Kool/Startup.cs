﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using E_Kool.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity.UI.Services;
using E_Kool.Services;
using E_Kool.Models;
using AspNetCore.Models;

namespace E_Kool
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, ApplicationDbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            CreateRoles(serviceProvider, context).Wait();
        }
        private async Task CreateRoles(IServiceProvider serviceProvider, ApplicationDbContext context)
        {
            //initializing custom roles   
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            string[] roleNames = {"Student", "Principle", "Teacher", "Headteacher", "SubTeacher", "Parent", "Admin"  };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);// tagastab true või false
                if (!roleExist)
                {
                    //create the roles and seed them to the database: Question 1  
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName)); // saan teha rolle juurde
                }
            }

            var schoolClass = await context.SchoolClass.FirstOrDefaultAsync();
            if (schoolClass == null)
            {
                schoolClass = new SchoolClass() { ClassName = "Üld", ClassCode = 'A' };
                context.SchoolClass.Add(schoolClass);
                context.SaveChanges();
            }

            ApplicationUser user = await UserManager.FindByEmailAsync("jignesh@gmail.com");

            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = "jignesh@gmail.com",
                    Email = "jignesh@gmail.com",
                    FirstName = "AdminniToomas",
                    LastName = "ToomasAnni"
                };
                await UserManager.CreateAsync(user, "Test@123");
                await UserManager.AddToRoleAsync(user, "Admin");
            }

            ApplicationUser user1 = await UserManager.FindByEmailAsync("marge@ekool.ee");

            if (user1 == null)
            {
                user1 = new ApplicationUser()
                {
                    UserName = "marge@ekool.ee",
                    Email = "marge@ekool.ee",
                    FirstName = "Marge",
                    LastName = "Luri"
                };
                await UserManager.CreateAsync(user1, "Test@123");
                await UserManager.AddToRoleAsync(user1, "Principle");
            }

            ApplicationUser user2 = await UserManager.FindByEmailAsync("joonas.jarvelaine@ekool.ee");

            if (user2 == null)
            {
                user2 = new ApplicationUser()
                {
                    UserName = "joonas.jarvelaine@ekool.ee",
                    Email = "joonas.jarvelaine@ekool.ee",
                    FirstName = "Joonas",
                    LastName = "Järvelaine"
                };
                await UserManager.CreateAsync(user2, "Test@123");
                await UserManager.AddToRoleAsync(user2, "Student");

                var student = new Student() { FirstName = user2.FirstName, ApplicationUserId = user2.Id, SchoolClassId = schoolClass.Id };
                context.Student.Add(student);
                context.SaveChanges();
            }

            ApplicationUser user3 = await UserManager.FindByEmailAsync("peeter@ekool.ee");

            if (user3 == null)
            {
                user3 = new ApplicationUser()
                {
                    UserName = "peeter@ekool.ee",
                    Email = "peeter@ekool.ee",
                    FirstName = "Peeter",
                    LastName = "Meeter"
                };
                await UserManager.CreateAsync(user3, "Test@123");
                await UserManager.AddToRoleAsync(user3, "Teacher");
            }

            ApplicationUser user4 = await UserManager.FindByEmailAsync("annika@ekool.ee");

            if (user4 == null)
            {
                user4 = new ApplicationUser()
                {
                    UserName = "annika@ekool.ee",
                    Email = "annika@ekool.ee",
                    FirstName = "Annika",
                    LastName = "Panniga"
                };
                await UserManager.CreateAsync(user4, "Test@123");
                await UserManager.AddToRoleAsync(user4, "Headteacher");

            }
        }
    }
}
