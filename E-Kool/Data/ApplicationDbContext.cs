﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AspNetCore.Models;
using E_Kool.Models;
using System.Collections;

namespace E_Kool.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<AspNetCore.Models.Student> Student { get; set; }
        public DbSet<AspNetCore.Models.Teacher> Teacher { get; set; }
        public DbSet<AspNetCore.Models.Subject> Subject { get; set; }

        public DbSet<AspNetCore.Models.SchoolClass> SchoolClass { get; set; }
        public DbSet<E_Kool.Models.TeacherSubject> TeacherSubject { get; set; }
        public DbSet<E_Kool.Models.SchoolClassSubjectTeacher> SchoolClassSubjectTeacher { get; set; }
        public DbSet<E_Kool.Models.Principle> Principle { get; set; }
        public DbSet<E_Kool.Models.HeadTeacher> HeadTeacher { get; set; }
        public DbSet<E_Kool.Models.Parents> Parents { get; set; }
        public DbSet<E_Kool.Models.Parents> Admin { get; set; }
        public DbSet<E_Kool.Models.Grade> Grade { get; set; }
        public DbSet<E_Kool.Models.StudentSubjectGrades> StudentSubjectGrades { get; set; }
        public DbSet<E_Kool.Models.SchoolClassSubjects> SchoolClassSubjects { get; set; }

    }
}
